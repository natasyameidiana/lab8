from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.core import serializers
from django.contrib.auth import authenticate, login, logout
import requests
import json

# Create your views here.
def searchbook(request):
    if request.user.is_authenticated:
        request.session['user'] = request.user.username
        request.session['email'] = request.user.email
        count = request.session.get('counter', 0)
        request.session['counter'] = count + 1
        for key, value in request.session.items():
            print('{} = {}'.format(key, value))
    return render(request, 'searchbook.html')


def searchJson(request):
	searching =  request.GET.get('cari')	
	url = "https://www.googleapis.com/books/v1/volumes?q=" + searching
	data = requests.get(url).json()
	if request.user.is_authenticated:
		count = request.session.get('counter',0)
		request.session['counter'] = count
		for key, value in request.session.items():
			print('{} => {}'.format(key,value))
	return JsonResponse(data)

    
def logout_view(request):
	request.session.flush()
	logout(request)
	return HttpResponseRedirect('/searchbook')

def increasefav(request):
	print(dict(request.session))
	request.session['counter'] = request.session['counter'] + 1
	return HttpResponse(request.session['counter'], content_type='application/json')

def decreasefav(request):
	#print(dict(request.session))
	request.session['counter'] = request.session['counter'] - 1
	return HttpResponse(request.session['counter'], content_type='application/json')
