from django.test import TestCase
from django.test import Client
from django.urls import resolve
from . import views
from .views import searchbook, searchJson, increasefav, decreasefav,logout_view
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.


class Lab9UnitTest(TestCase):
	def test_lab_9_list_book_url_is_exist(self):
		response = Client().get('/searchbook')
		self.assertEqual(response.status_code, 200)

	def test_lab_9_json_file_is_exist(self):
		response = Client().get('/searchJson?cari=')
		self.assertEqual(response.status_code, 200)

	def test_lab_9_using_books_template(self):
		response = Client().get('/searchbook')
		self.assertTemplateUsed(response, 'searchbook.html')

	def test_lab11_using_func(self):
		found = resolve('/searchbook')
		self.assertEqual(found.func, searchbook)

	def test_lab_11_using_increasefav(self):
		found = resolve('/increasefav')
		self.assertEqual(found.func, increasefav)

	def test_lab_11_using_decreasefav(self):
		found = resolve('/decreasefav')
		self.assertEqual(found.func, decreasefav)

	def test_landing_page(self):
		response = Client().get('/searchbook')
		html_response = response.content.decode('utf-8')
		self.assertIn('List Of Books', html_response)

	def test_11_landing_page(self):
		response = Client().get('/searchbook')
		html_response2 = response.content.decode('utf-8')
		self.assertIn('Favourite your Books Here!', html_response2)	
