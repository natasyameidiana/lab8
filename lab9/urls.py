from django.urls import path
from django.contrib import admin
from . import views 
from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings
from .views import *


app_name = 'lab9'

urlpatterns = [
	path('searchbook', views.searchbook, name="home"),
	path('searchJson', views.searchJson, name="searchJson"),
        path('increasefav', views.increasefav, name='increasefav'),
	path('decreasefav', views.decreasefav, name='decreasefav'),
	path('logout', views.logout_view , name='logout'),

]
