$(document).ready(function() {
    var name = "";
    var password = "";
    $("#id_name").change(function() {
        name = $(this).val();
    });
    $("#id_password").change(function() {
        password = $(this).val();
    })
    $("#id_email").change(function() {
        var email = $(this).val();
        console.log(email)
        $.ajax({
            url: "/validate",
            data: {
                'email': email,
            },
            method: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data)
                if(data.not_valid) {
                    alert("This email has subscribed this channel");
                } else {
                    if(name.length > 0 && password.length > 5) {
                        $("#submit").removeAttr("disabled");
                    }
                }
                
            }
        });
    });
    $("#submit").click(function() {
        event.preventDefault();
        $.ajax({
            headers: {"X-CSRFToken": $("input[name=csrfmiddlewaretoken]").val()},
            url:"/success",
            data: $("form").serialize(),
            method: 'POST',
            dataType: 'json',
            success: function(data){
                swal({
                    title: "Hello " + data.name,
                    text: 'Thanks for subscribing. Redirecting you back to home...',
                    type: 'success',
                    showConfirmButton: false,
                    timer: 2500
                    footer: '<a href=\"/subscribers\">See all subscribers</a>'
                    }).then(function() {
                        window.location = "/"
                    });   
            }, error: function() {
                swal({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!',
                    showConfirmButton: false,
                    timer: 2500
                  }).then(function() {
                      window.location = "/"
                  });
            }
        })
    });
});


    $.ajax({
        method: "GET",
        url: 'list',
        success: function (data) {
            if (data.length > 0){
                var tbody = '';
                for (let i = 0; i < data.length; i++){
                    var name = data[i]['name'];
                    var email = data[i]['email'];
                    tbody = tbody
                            + '<tr>'
                            + '<td class="align-middle">' + name + '</td>'
                            + '<td class="align-middle">' + email + '</td>'
                            + '<td class="align-middle">'
                            + '<button type="submit" class="btn-danger unsubscribe" data-email=' + email + '>'
                            + 'Unsubscribe</button></td>'
                }
                $(tbody).appendTo('#myTable');
            }
        }
    })

    $('#myTable').on('click', 'td .unsubscribe', function (e) {
            var person = prompt("Please enter your password");
            e.preventDefault();
            var email = $(this).attr('data-email');
            $.ajax({
                url: 'unsubs/' + email + "/" + person + "/",
                method: 'POST',
                dataType: 'json',
                data: {'email': email},
                success: function () {
                    location.reload();
                }
            })
        })



});
