var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 1000);
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}

$( function() {
	$( "#accordion" ).accordion({
		collapsible: true
    });
} );

$(document).ready(function(){
	$("#switch").click(function(){
		if ($(this).is(":checked")) {
			$("body").css("background","#00004d");
          $("h1").css("color", "white");
          $("label").css("color", "white");
        } else{
          $("body").css("background","linear-gradient(to top, #ffcc00 0%, #ff3300 100%)");
          $("h1").css("color", "black");
        }
      })
    });

